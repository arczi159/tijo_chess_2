package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.ChessService;

import java.util.Arrays;

@Service
public class ChessServiceImpl implements ChessService {

    private RulesOfGame pawn;
    private RulesOfGame king;
    private RulesOfGame rook;
    private RulesOfGame queen;
    private RulesOfGame bishop;
    private RulesOfGame knight;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("Rook") RulesOfGame rook,
                            @Qualifier("King") RulesOfGame king,
                            @Qualifier("Pawn") RulesOfGame pawn){
        this.bishop = bishop;
        this.knight = knight;
        this.queen = queen;
        this.rook=rook;
        this.king=king;
        this.pawn=pawn;



    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        int[] currentPosition = Arrays.stream(figureMoveDto.getStart().split("_"))
                .mapToInt(position -> position.charAt(0))
                .toArray();

        int[] destinationPosition = Arrays.stream(figureMoveDto.getDestination().split("_"))
                .mapToInt(position -> position.charAt(0))
                .toArray();

        switch(figureMoveDto.getType()){
            case BISHOP:
                return bishop.isCorrectMove(currentPosition[0],currentPosition[1],destinationPosition[0],destinationPosition[1]);
            case KING:
                return king.isCorrectMove(currentPosition[0],currentPosition[1],destinationPosition[0],destinationPosition[1]);
            case PAWN:
                return pawn.isCorrectMove(currentPosition[0],currentPosition[1],destinationPosition[0],destinationPosition[1]);
            case ROCK:
                return rook.isCorrectMove(currentPosition[0],currentPosition[1],destinationPosition[0],destinationPosition[1]);
            case QUEEN:
                return queen.isCorrectMove(currentPosition[0],currentPosition[1],destinationPosition[0],destinationPosition[1]);
            case KNIGHT:
                return knight.isCorrectMove(currentPosition[0],currentPosition[1],destinationPosition[0],destinationPosition[1]);
        }
        return false;
    }
}
