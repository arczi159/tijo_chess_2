package pl.edu.pwsztar.domain.chess;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RookTest {
    private RulesOfGame rook = new RulesOfGame.Rook();
    @Tag("Rock")
    @ParameterizedTest
    @CsvSource({
            "2, 1, 2, 4",
            "6, 1, 8, 1",
            "3, 2, 8, 2",
            "1, 4, 3, 4",
            "3, 4, 3, 1"
    })
    void checkCorrectMoveForRock(int xStart, int yStart, int xStop, int yStop){
        assertTrue(rook.isCorrectMove(xStart, yStart, xStop, yStop));
    }
    @ParameterizedTest
    @CsvSource({
            "2, 2, 3, 3",
            "4, 4, 4, 4"
    })
    void checkIncorrectMoveForRock(int xStart, int yStart, int xStop, int yStop){
        assertFalse(rook.isCorrectMove(xStart, yStart, xStop, yStop));
    }
}